﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integration
{
    interface IFormula
    {
        double GetY(double x);
    }

    class SomeFormula : IFormula
    {
        public double GetY(double x)
        {
            return Math.Sqrt(x);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            SomeFormula formula = new SomeFormula();

            double Cvad = kvadr(0, 16, 4, 100, formula);
            Console.WriteLine("Площа за квадратом: ");
            Console.WriteLine(Cvad.ToString("0.000") + "\n");
            Console.WriteLine(new string('-',40));

            double vKarmo = Karmo(0, 16, 4, 10_000, formula);
            Console.WriteLine("Площа за Монте Карло: ");
            Console.WriteLine(vKarmo.ToString("0.000"));

            Console.ReadLine();
        }

        static double kvadr(double a, double b, double c, int countSegments, IFormula f)
        {
            double width = b - a;
            double height = c;

            double h = width / countSegments;

            double area = 0;

            for (int i = 0; i < countSegments; i++)
            {
                area += f.GetY(a + h * i) * h;
            }

            return area;
        }
        
        static double Karmo(double a, double b, double c, int countDots, IFormula f)
        {
            double width = b - a;
            double height = c;

            int countDotsUnderCurve = 0;

            Random rand = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i < countDots; i++)
            {
                double x = rand.NextDouble() * (b - a) + a;
                double y = rand.NextDouble() * c;

                if(f.GetY(x) > y) countDotsUnderCurve++;
            }

            return (width * height * ((double)countDotsUnderCurve / countDots));
        }
    }
}
