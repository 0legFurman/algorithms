﻿#include <iostream>
#include <ctime>
#include <conio.h>
#include <windows.h>
#include <math.h>
#include <string>


using namespace std;
void PBinary(int number);

struct date {
	unsigned short Sekonds : 7;     
	unsigned short Minutes : 7;    
	unsigned short Hours : 6;    
	unsigned short MonthDay : 6;    
	unsigned short WeekDay : 3;     	
	unsigned short Month : 5;    
	unsigned short Year : 8;    

	date(unsigned short nSeconds,
		unsigned short nMinutes,
		unsigned short nHours,
		unsigned short nWeekDay,
		unsigned short nMonthDay,
		unsigned short nMonth,
		unsigned short nYear)
	{
		this->Sekonds = nSeconds;
		this->Minutes = nMinutes;
		this->Hours = nHours;
		this->WeekDay = nWeekDay;
		this->MonthDay = nMonthDay;
		this->Month = nMonth;
		this->Year = nYear;
	}

	void PrintDate()
	{
		cout << "----------------------------" << endl << "Рік :" << Year << endl;
		cout << "Місяць :" << Month << endl;
		cout << "День місяця :" << MonthDay << endl;
		cout << "День неділі :" << WeekDay << endl;
		cout << "Годин :" << Hours << endl;
		cout << "Хвилин :" << Minutes << endl;
		cout << "Секунд :" << Sekonds << endl << "----------------------------" << endl;
	}

	date()
	{}
};

union sSh
{
	signed short number = +50;

	struct divided
	{
		unsigned short number : 7;
		unsigned short sign : 1;

	} d;

	void rez()
	{
		cout << "----------------------------" << endl << "Знак: " << (d.sign == 0 ? '+' : '-') << endl;
		cout << "Значення: " << (d.sign == 0 ? d.number : 128 - d.number) << endl << "----------------------------" << endl;
	}

	void rLog()
	{
		unsigned short sign = number & 0b10000000;		
		unsigned short myNumber = number & 0b01111111;	

		cout << "----------------------------" << endl << "Знак: " << (sign == 0 ? '+' : '-') << endl;
		cout << endl << "Значення: " << (sign == 0 ? myNumber : 128 - myNumber) << endl << "----------------------------" << endl;
	}
};

union myFloat
{
	float f = 16;

	long l;

	struct {
		unsigned int frac : 23;
		unsigned int exp : 8;
		unsigned int sign : 1;
	} bin;

	void sBits()
	{
		for (int i = 32; i > 0; --i)
		{
			cout << (int)((l >> i - 1) & 1) << " ";
		}
		cout << endl << endl;
	}

	void sBytes()
	{
		for (int i = 32; i > 0; i -= 8)
		{
			cout << (int)((l >> i - 8) & 0b11111111) << " "; 
		}
		cout << endl << endl;
	}

	void sInfo()
	{
		cout << "----------------------------" << endl << "Знак: " << (bin.sign == 0 ? '+' : '-') << endl;
		cout << "Значення: " << bin.frac << endl;
		cout << "Степінь: " << bin.exp << endl << "----------------------------" << endl;
	}
};

int main()
{
	setlocale(LC_ALL, "ru");

	SYSTEMTIME time;
	GetLocalTime(&time);

	date d = date(time.wSecond, time.wMinute, time.wHour, time.wDayOfWeek, time.wDay, time.wMonth, time.wYear % 100);
	d.PrintDate();


	sSh number;
	number.rez();
	number.rLog();


	myFloat mf;
	mf.sBits();
	mf.sBytes();
	mf.sInfo();


	signed char test;
	unsigned char uTest;

	test = 5 + 127;
	cout << "5 + 127 = " << (int)test << endl;
	PBinary(test);

	test = 2 - 3;
	cout << "2 - 3 = " << (int)test << endl;
	PBinary(test);

	test = -120 - 34;
	cout << "-120 - 34 = " << (int)test << endl;
	PBinary(test);

	uTest = (unsigned char)-5;
	cout << "-5 = " << (int)test << endl;
	PBinary(uTest);

	test = 56 & 38;
	cout << "56 & 38 = " << (int)test << endl;
	PBinary(test);

	test = 56 | 38;
	cout << "56 | 38 = " << (int)test << endl;
	PBinary(test);
	int k = 0;
	cin >> k;
	return 0;
}

void PBinary(int number)
{
	for (int i = 8; i >= 0; --i)
	{
		cout << (int)((number >> i) & 1);
	}

	cout << endl << endl;
}
