﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sort;

namespace UnitTestSort
{
    [TestClass]
    public class UnitTest1
    {
        public static List<int> SomeMass500 = new List<int>();
        public static List<int> SomeMass10000 = new List<int>();
        public static List<int> SomeMass25000 = new List<int>();

        public static LinkedList<int> SomeList500 = new LinkedList<int>();
        public static LinkedList<int> SomeList10000 = new LinkedList<int>();
        public static LinkedList<int> SomeList25000 = new LinkedList<int>();

        

        public void someMass(List<int> mas, int count)
        {
            var rnd = new Random((int)DateTime.Now.Ticks);

            for (int i = 0; i < count; i++)
            {
                mas.Add(rnd.Next(0, 10));
            }
            
        }

        public void someFille(LinkedList<int> list, int count)
        {
            var random = new Random((int)DateTime.Now.Ticks);

            for (int i = 0; i < count; i++)
            {
                list.AddLast(random.Next(0, 10));
            }
        }

        [TestInitialize]
        public void Prepare()
        {
            someMass(SomeMass500, 500);
            someMass(SomeMass10000, 10000);
            someMass(SomeMass25000, 25000);

            someFille(SomeList500, 500);
            someFille(SomeList10000, 10000);
            someFille(SomeList25000, 25000);
        }

        [TestMethod]
        public void SomeMyTest()
        {

        }

        
            [TestMethod]
            public void SelectionSort500()
            {
                Sort.Program.SelectionSort(SomeMass500);
            }

            [TestMethod]
            public void SelectionSort10000()
            {
                Sort.Program.SelectionSort(SomeMass10000);
            }

            [TestMethod]
            public void SelectionSort25000()
            {
                Sort.Program.SelectionSort(SomeMass25000);
            }
       

      
            [TestMethod]
            public void InsertionSort500()
            {
                Sort.Program.InsertionSort(SomeMass500);
            }

            [TestMethod]
            public void InsertionSort10000()
            {
                Sort.Program.InsertionSort(SomeMass10000);
            }

            [TestMethod]
            public void InsertionSort25000()
            {
                Sort.Program.InsertionSort(SomeMass25000);
            }
    

       
            [TestMethod]
            public void InsertionSortForList500()
            {
                Sort.Program.InsertionSortForList(SomeList500);
            }

            [TestMethod]
            public void InsertionSortForList10000()
            {
                Sort.Program.InsertionSortForList(SomeList10000);
            }

            [TestMethod]
            public void InsertionSortForList25000()
            {
                Sort.Program.InsertionSortForList(SomeList25000);
            }
      

       
            [TestMethod]
            public void ShellSort500()
            {
                Sort.Program.ShellSort(SomeMass500);
            }

            [TestMethod]
            public void ShellSort10000()
            {
                Sort.Program.ShellSort(SomeMass10000);
            }

            [TestMethod]
            public void ShellSort25000()
            {
                Sort.Program.ShellSort(SomeMass25000);
            }
    }
}
