﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuplexLinkedList
{
    class D<T> : IEnumerable<T>
    {
        public SomeListItem<T> head;
        public SomeListItem<T> tail;

        public int count = 0;

        public D() { }

        public D(T startData)
        {
            Addend(startData);
        }

        public D(IEnumerable<T> startData)
        {
            foreach(T item in startData)
                Addend(item);
        }
        public T Popend()
        {
            T data = tail.data;

            tail = tail.previous;
            tail.next = null;

            count--;

            return data;
        }


        public void AddStart(T data)
        {
            SomeListItem<T> item = new SomeListItem<T>(data);

            if (count == 0)
                Addend(data);
            else
            {
                item.next = head;
                head.previous = item;
                head = item;
                count++;
            }            
        }

     
        public void Addend(T data)
        {
            SomeListItem<T> item = new SomeListItem<T>(data);

            if (count == 0)
            {
                head = item;
                tail = item;
            }
            else
            {
                tail.next = item;
                item.previous = tail;
                tail = item;
            }
            count++;
        }

        public T PopFront()
        {
            T data = head.data;

            head = head.next;
            head.previous = null;

            count--;

            return data;
        }

        public bool ADelete(T data)
        {
            var current = head;

            while(current != null)
            {
                if(current.data.Equals(data))
                {
                    current.previous.next = current.next;
                    current.next.previous = current.previous;
                    count--;
                    return true;
                }
            }

            return false;
        }

        public void DeleteList()
        {
            tail = null;
            head = null;
            count = 0;
        }

        public void printList()
        {
            foreach(T item in this)
                Console.WriteLine(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new ListNumeration<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
